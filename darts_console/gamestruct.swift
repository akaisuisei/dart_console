//
//  gamestruct.swift
//  darts_console
//
//  Created by kamwa watanabe bjay on 2/9/17.
//  Copyright © 2017 kamwa watanabe bjay. All rights reserved.
//

import Foundation

struct Gamestruct {
    var init_score : Int = 501;
    var player_num : Int = 1;
    var team_num : Int = 0;
    var players = [player]();
    var teams = [Team]();
    var turn : Int = 0;
}

func readNumber(description:String, default_nb:Int) -> Int
{
    var buff : String?;
    var res:Int = default_nb;
    var tmp : Int?;
    repeat {
        print(description)
        buff = readLine();
        if (buff == "") {
            break;
        }
        tmp = Int(buff!);
    } while (tmp == nil || tmp! < 0);
    if (buff != "") {
        res = tmp!;
    }
    return res;
}


func readScore(description:String) -> Int
{
    var buff : String?;
    var tmp : Int?;
    repeat {
        print(description)
        buff = readLine();
        if (buff == "change") {
            _ = print_changeScore();
        } else if (buff == "print") {
            print(gamestatus.players);
        } else if (buff == "") {
            break;
        }
        tmp = Int(buff!);
    } while (tmp == nil || tmp! < 0);
    if (buff == ""){
        return 0;
    }
    return tmp!;
}

func changeScore(playerName:String, tour:Int, new_val:Int) -> Bool
{
    let p : player = gamestatus.players.first(where: {$0.name == playerName})!;
    return p.change_score(index: tour, val: new_val);
}

func print_changeScore()-> Bool
{
    print("entering change mode\nenter player name:");
    let playerName : String? = readLine();

    let p : player? = gamestatus.players.first(where: {$0.name == playerName});
    if (p == nil) {
        print("error");
        return false;
    }
    print(p as Any);
    let tour = readNumber(description: "enter throw you want to change:",
                          default_nb: 0);
    if (tour > (p?.score_table.count)! - 1) {
        print("error");
        return false;
    }
    
    let newVal = readNumber(description: "enter new score",
                            default_nb: (p?.score_table[tour])!);
    let res = p?.change_score(index: tour, val: newVal);
    if (res)!{
        print("change succefull\nnew score: \(p?.get_score)");
    } else {
        print("error");
    }
    return res!;
}

func initialize(gamestatus: inout Gamestruct)->Void{
    gamestatus.team_num = readNumber(
        description: "enter the number of team(s):",
        default_nb: gamestatus.team_num)
    gamestatus.player_num = readNumber(
        description: "enter the number of player(s):",
        default_nb: gamestatus.player_num)
    repeat {
        gamestatus.init_score = readNumber(
            description: "enter starting point : 301, 501, 1001",
            default_nb: 501)
    } while (gamestatus.init_score != 501
        && gamestatus.init_score != 301
        && gamestatus.init_score != 1001);
    
    if (gamestatus.team_num == 0){
        for i in 0 ..< gamestatus.player_num {
            print("enter Player \(i ) Name:");
            var name = readLine()!;
            name = name  == "" ? "Player \(i )" : name;
            let play = player(name : name, score: gamestatus.init_score);
            gamestatus.players.append(play);
        }
    }
    else{
        let playerByTeam = gamestatus.player_num / gamestatus.team_num;
        for i in 0 ..< gamestatus.player_num {
            if (i % playerByTeam == 0){
                print("enter Team \(i / playerByTeam ) Name:");
                var name = readLine()!;
                name = name  == "" ? "Player \(i )" : name;
                let team = Team(name : name, score: gamestatus.init_score);
                gamestatus.teams.append(team);
            }
            print("enter Player \(i ) Name:");
            var name = readLine()!;
            name = name  == "" ? "Player \(i )" : name;
            gamestatus.teams.last?.addTeammate(name: name);
        }
    }
    print("\(gamestatus.init_score ) game start");
}

func play_with_player(gamestatus: inout Gamestruct)->Void{
    var has_win :Int = 0;
    if (gamestatus.player_num == 1){
        gamestatus.player_num = 2;
    }
    
    while(has_win != gamestatus.player_num - 1){
        has_win = 0;
        for play in gamestatus.players{
            if (play.hasWin()){
                has_win += 1;
                continue;
            }
            let score = readScore(description: "\(play.name ) : \(play.get_score )");
            if (!play.add_score(point: score)){
                print("burst: \(play.name )");
            }
            has_win += play.hasWin() ? 1 : 0;
            if (has_win == gamestatus.player_num - 1){
                break;
            }
            print("\(play.name ) new score: \(play.get_score )");
        }
    }
    print(gamestatus.players);
}

func play_with_team(gamestatus: inout Gamestruct)->Void{
    let playerByTeam = gamestatus.player_num / gamestatus.team_num;
    var has_win :Int = 0;
    var tmp: Int = 0;
    while(has_win != gamestatus.team_num - 1){
        has_win = 0;
        for play in gamestatus.teams{
            if (play.hasWin()){
                has_win += 1;
                continue;
            }
            
            let score = readScore(description: "\(play.teammateName[tmp] )"
                + "(\(play.name )): \(play.get_score )");
            if (!play.add_score(point: score)){
                print("burst: \(play.teammateName[tmp] )(\(play.name ))");
            }
            
            has_win += play.hasWin() ? 1 : 0;
            if (has_win == gamestatus.team_num - 1){
                break;
            }
            print("\(play.teammateName[tmp] )(\(play.name )) "
                + "new score: \(play.get_score )");
        }
        tmp = (1 + tmp) % playerByTeam;
        gamestatus.turn += 1;
    }
    print(gamestatus.teams);
}
