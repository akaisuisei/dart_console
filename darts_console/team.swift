//
//  team.swift
//  darts_console
//
//  Created by kamwa watanabe bjay on 2/9/17.
//  Copyright © 2017 kamwa watanabe bjay. All rights reserved.
//

import Foundation

class Team :player {
    var teammateName: [String] = [];
    
    func addTeammate(name: String) -> Void {
        teammateName.append(name);
    }
    
    override var description: String{
        let count : Int = teammateName.count;
        var res : String = "team: \(name )\n";
        for (index, element) in teammateName.enumerated()
        {
            let aux = stride(from: index, to: score_table.count, by: count).map { score_table[$0] };
            res += "player : \(element )\n"
            + "\(player.scoreToString(array: aux, mul: count))";
        }
        return res;
    }
    
}
