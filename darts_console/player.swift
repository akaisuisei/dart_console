//
//  player.swift
//  darts_console
//
//  Created by kamwa watanabe bjay on 2/9/17.
//  Copyright © 2017 kamwa watanabe bjay. All rights reserved.
//

import Foundation

class player : CustomStringConvertible{
    
    let name: String;
    private var score: Int = 0;
    var get_score:Int{return score;};
    internal var score_table: [Int] = [];
    static var init_score:Int = 0;
    
    init(name: String, score: Int) {
        self.name = name;
        self.score = score;
        player.init_score  = score;
    }
    
    func add_score(point:Int) -> Bool {
        score_table.append(point);
        if (score < point){
            return false;
        }
        score -= point;
        return true;
    }
    
    func hasWin() -> Bool{
        return score == 0;
    }
    
    func change_score(index: Int, val:Int)->Bool{
        let aux: Int = val - score_table[index];
        var res: Bool = true;
        if (val < 0 || score - aux < 0){
            return false;
        }
        score_table[index] = val;
        for (_, element) in score_table.enumerated()
        {
            res = res && add_score(point: element);
        }
        return res;
    }
    
    internal static func scoreToString(array: [Int], mul : Int) ->String{
        var score_list: String = "";
        var best:(Int, Int) = (0,0);
        var sum : Float = 0;
        for (index, element) in array.enumerated()
        {
            score_list += "throw: \(index ), Point: \(element )\n";
            if (element > best.1)
            {
                best.0 = index;
                best.1 = element;
            }
            sum += Float(element);
        }
        sum /= Float(array.count);
        return "best score: \(best.1 )\naverage: \(sum ) \n\(score_list )";
    }
    
    var description: String{
        return "\(name ) \n\(player.scoreToString(array: score_table, mul: 1))"
    }
}
